 /* ********************** DECLARO LAS VARIABLES**********************  */
var s0=1;
var s1=1;
var m0=1;
var m1=1;
var h0=1;
var h1=1;

/*  ********************** CARGO EL BODY  **********************  */
window.addEventListener("load", () => {

    /*  ********************** inicializo el reloj a 0.  **********************  */	
    var dig = document.querySelectorAll(".digitos");
    for (var i = 0; i < dig.length; i++) {
        dig[i].innerHTML = "0";
    }
    /* ********************** SEGUNDOS  ********************** */
    setInterval(()=>{
    	document.querySelector("#s0").innerHTML=s0;
    	s0++;
    	if(s0==10){
    		s0=0;
    	}
    },1000);

    setInterval(()=>{
    	document.querySelector("#s1").innerHTML=s1;
    	s1++;
    	if(s1==6){
    		s1=0;
    	}
    },10000);

    /*  **********************  MINUTOS  **********************  */
    setInterval(()=>{
    	document.querySelector("#m0").innerHTML=m0;
    	m0++;
    	if(m0==10){
    		m0=0;
    	}
    },60000);

    setInterval(()=>{
    	document.querySelector("#m1").innerHTML=m1;
    	m1++;
    	if(m1==6){
    		m1=0;
    	}
    },600000);

     /*  **********************  HORAS  **********************  */

     setInterval(()=>{
     	document.querySelector("#h0").innerHTML=h0;
     	h++;
     	if(h0==10){
     		h0=0;
     	}
     },3600000);

     setInterval(()=>{
     	document.querySelector("#h1").innerHTML=h1;
     	h1++;
     	if(h1==3){
     		h1=0;
     	}
     },36000000)

})