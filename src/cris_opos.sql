DROP DATABASE if EXISTS cris_opos;
CREATE DATABASE cris_opos;
USE cris_opos;
CREATE TABLE preguntas(
  id_pregunta int AUTO_INCREMENT,
  pregunta text,
  op1 text,
  op2 text,
  op3 text,
  op4 text,
  respuesta int,
  tema int,
  INDEX(tema),
  PRIMARY KEY(id_pregunta)
  );
INSERT INTO preguntas (pregunta, op1, op2, op3, op4, respuesta,tema)
  VALUES (
  '(99/2003) Si varios ordenadores est�n conectados en red pueden (se�alar la respuesta incorrecta)',
  'Conectarles con un ordenador que no est� dentro de la red',
  'Compartir un esc�ner',
  'Compartir una impresora',
  'Compartir informaci�n entre ellos',
  1,18),(
  '(100/2003) En t�rminos inform�ticos �Que es una carpeta?',
  'Un documento de texto',
  'Espacio del disco duro de un ordenador destinado a almacenar informaci�n del usuario',
  'Una p�gina de una hoja de calculo',
  'Una parte de la base de datos',
  2,18),(
  '(85/2007) Durante su ejecuci�n, un programa reside en:',
  'En la pantalla del ordenador',
  'La memoria principal del ordenador',
  'El bus',
  'El disco duro',
  2,18),(
  'Se�ala la correcta:',
  'los poderes p�blicos est�n sujetos a la Constituci�n y al resto del ordenamiento jur�dico',
  'los ciudadanos est�n sujetos a la Constituci�n y al resto del ordenamiento jur�dico',
  'los ciudadanos y los poderes p�blicos est�n sujetos a la Constituci�n',
  'los ciudadanos y los poderes p�blicos est�n sujetos a la Constituci�n y al resto del ordenamiento jur�dico',
  4,1),(
  'Regular� las bases de la organizaci�n militar conforme a los principios de la presente Constituci�n:',
  'una ley org�nica',
  'un decreto ley',
  'una norma con rango de ley',
  'una ley de bases',
  1,1),(
  'La bandera de Espa�a est� formada por:',
  'tres franjas horizontales, roja, amarilla y roja, siendo la amarilla de doble anchura que cada una de las rojas',
  'tres franjas horizontales, amarilla, roja y amarilla, siendo la roja la mitad que cada una de las amarillas',
  'tres franjas horizontales, roja, amarilla y roja, siendo la amarilla la mitad que cada una de las rojas',
  'tres franjas horizontales, amarilla, roja y amarilla, siendo la roja de doble anchura que cada una de las amarillas',
  1,1),(
  'Tiene como misi�n garantizar la soberan�a e independencia de Espa�a, defender su integridad territorial y el ordenamiento constitucional:',
  'el Gobierno',
  'el Jefe de Estado',
  'los tribunales',
  'las Fuerzas Armadas',
  4,1),(
  'La forma pol�tica del Estado espa�ol es:',
  'la Monarqu�a parlamentaria',
  'la soberan�a nacional',
  'el pluralismo pol�tico',
  'la sociedad democr�tica',
  1,1),
  (
  'El Jefe de Estado es',
  'el Rey',
  'el Presidente del Gobierno',
  'el Presidente del Congreso',
  'el Defensor del Pueblo',
  '1',
  '2'
  ),
  (
  'Extinguidas todas las l�neas llamadas en Derecho, proveer� a la sucesi�n en la Corona en la forma que m�s convenga a los intereses de Espa�a:',
  'el Congreso de los Diputados',
  'el Senado',
  'las Cortes Generales',
  'el Presidente del Gobierno',
  '3',
  '2'
  ),
  (
  'Las abdicaciones y renuncias y cualquier duda de hecho o de derecho que ocurra en el orden de sucesi�n a la Corona se resolver�n:',
  'mediante Decreto-Ley',
  'por Ley Org�nica',
  'por el Presidente del Gobierno',
  'mediante refer�ndum',
  '2',
  '2'
  ),
  (
  'Para ejercer la Regencia es preciso:',
  'ser espa�ol de nacimiento y mayor de 21 a�os',
  'ser espa�ol y mayor de edad',
  'ser espa�ol de nacimiento y mayor de edad',
  'ser espa�ol y mayor de 21 a�os',
  '2',
  '2'
  ),

(
  'Corresponde aprobar los Presupuestos del Estado a:',
  'el Rey',
  'las Cortes Generales',
  'el Congreso de los Diputados',
  'el Presidente del Gobierno',
  '2',
  '3'
  ),
(
  'Se�ala la correcta:',
  'nadie podr� ser miembro de las dos C�maras simult�neamente',
  'es compatible el acta de una Asamblea de Comunidad Aut�noma con la de Diputado al Congreso',
  'los miembros de las Cortes Generales estar�n ligados por mandato imperativo',
  'las reuniones de Parlamentarios que se celebren sin convocatoria reglamentaria vincular�n a las C�maras',
  '1',
  '3'
  )/*,
(
  '',
  '',
  '',
  '',
  '',
  '',
  ''
  )*/

  ;

SELECT * FROM preguntas
  WHERE tema BETWEEN 1 AND 3
  ORDER BY RAND() LIMIT 5;